package com.example.recyclerviewswipedloadjson

import SwipeHelper
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.MenuItemCompat
import androidx.core.view.doOnAttach
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewswipedloadjson.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

lateinit var deleteIcon: Drawable
class MainActivity : AppCompatActivity(),View.OnLongClickListener {
    lateinit var itemList : MutableList<Item>
    lateinit var selectionList : MutableList<Item>
    var selectionListPosition = ArrayList<Int>()
    lateinit var rvAdapter:RvAdapter
    lateinit var colorDrawableBackground: ColorDrawable
    lateinit var binding:ActivityMainBinding

    var isContexualModeEnabled=false
    var counter=0
    var defaultText="0 item selected"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.search_menu)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        itemList=ArrayList<Item>()
        selectionList=ArrayList<Item>()

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvAdapter = RvAdapter(itemList,this@MainActivity)

        recyclerview.apply {
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }

        recyclerview.layoutManager = layoutManager
        recyclerview.adapter = rvAdapter
        getJsondata()
        setUpRecyclerView()

    }
    private fun getJsondata(){
        try {
            val loadJson=JavaFunctions.ReadFile("main_data.json",this)
            val jsonObject=JSONObject(loadJson)
            val jsonArray=jsonObject.getJSONArray("users")

            Log.d("TAGJSON", "${jsonArray.length()}")

            for (i in 0 until jsonArray.length() ){
                val user : JSONObject = jsonArray.getJSONObject(i)

                val id = user.getInt("id")
                val firstName= user.getString("first_name")
                val lastName=user.getString("last_name")
                val email=user.getString("email")
                val gender=user.getString("gender")
                val car=user.getString("car")

//                Log.d("TAGJSON", "${id}")
//                Log.d("TAGJSON", "${user.getString("first_name")}")
//                Log.d("TAGJSON", lastName)
//                Log.d("TAGJSON", email)
//                Log.d("TAGJSON", gender)
//                Log.d("TAGJSON", car)

                this.itemList.add(Item(id,firstName,lastName,email,gender,car))
            }

            Log.d("TAGJSON", "${itemList}")

            rvAdapter.notifyDataSetChanged()
            rvAdapter = RvAdapter(itemList,this@MainActivity)
            recyclerview.adapter=rvAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    private fun setUpRecyclerView() {
        recyclerview.adapter = rvAdapter
        recyclerview.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        val itemTouchHelper = ItemTouchHelper(object : SwipeHelper(recyclerview) {
            override fun instantiateUnderlayButton(position: Int): List<UnderlayButton> {
                val buttons: List<UnderlayButton>
                val deleteButton = deleteButton(position)
                val markAsUnreadButton = markAsUnreadButton(position)
                val archiveButton = archiveButton(position)

                buttons = listOf(deleteButton, markAsUnreadButton, archiveButton)

                return buttons
            }
        })

        itemTouchHelper.attachToRecyclerView(recyclerview)
    }

    @SuppressLint("ShowToast")
    private fun toast(text: String) {
        Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
    }
    // for swipe buttons
    private fun deleteButton(position: Int) : SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            this,
            "Delete",
            14.0f,
            android.R.color.holo_red_light,
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                val deleteItem = itemList[position]
                    itemList.removeAt(position)
                        rvAdapter.notifyItemRemoved(position)
                        Snackbar.make(
                            findViewById(android.R.id.content),
                            "Swiped item is removing",
                            Snackbar.LENGTH_LONG
                        )
                            .setAction("Undo") {
                                itemList.add(position, deleteItem)
                                rvAdapter.notifyItemInserted(position)
                            }.setActionTextColor(Color.parseColor("#097EC1")).show()
                }
            })
    }
    private fun markAsUnreadButton(position: Int) : SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            this,
            "Mark as unread",
            14.0f,
            android.R.color.holo_green_light,
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    toast("Marked as unread item clicked")
                }
            })
    }
    private fun archiveButton(position: Int) : SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            this,
            "Archive",
            14.0f,
            android.R.color.holo_blue_light,
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    toast("Archived item clicked")
                }
            })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater=menuInflater
        inflater.inflate(R.menu.search_menu,menu)

        val item=menu!!.findItem(R.id.search_view)
        val searchView=MenuItemCompat.getActionView(item) as SearchView
        searchView.doOnAttach {
            toast("salom")
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
            return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                rvAdapter.filter.filter(newText)
            return true
            }

        })
        searchView.maxWidth=900
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var isDelete=false
        if (item.itemId==R.id.delete_menu){
            rvAdapter.removeItem(selectionList)
            Log.d("TAGP", "${selectionList} ${selectionListPosition}()}")
            val snackbar=Snackbar.make(
                findViewById(android.R.id.content),
                "selected items are removing",
                Snackbar.LENGTH_LONG
            )
                .setAction("Undo") {
                    var j=0
                    Log.d("TAGP", "${selectionList} ${selectionListPosition.toString()}")
                    for (i in selectionListPosition) {
                        itemList.add(i,selectionList[j])
                        j++
                        rvAdapter.notifyItemInserted(i)
                        rvAdapter.notifyDataSetChanged()
                    }
                    isDelete=true
                }.setActionTextColor(Color.parseColor("#097EC1"))
            snackbar.show()
            if (isDelete){
                removeContexualMenu()
                isDelete=false
            }

        } else if (item.itemId==android.R.id.home){
            removeContexualMenu()
            rvAdapter.notifyDataSetChanged()
        }
        return true
    }
    fun selectItem(v:View,adapterPosition:Int){
        if ((v as CheckBox).isChecked){
        selectionList.add(itemList[adapterPosition])
        selectionListPosition.add(adapterPosition)
            counter++
        }else {
            selectionList.remove(itemList[adapterPosition])
            selectionListPosition.remove(adapterPosition)
            counter--
        }
        updateCounterUI()

}
    @SuppressLint("SetTextI18n")
    fun updateCounterUI(){
        binding.tvCounter.text="$counter Item selected"
    }
    fun removeContexualMenu(){
        isContexualModeEnabled=false
        binding.apply{

            tvCounter.visibility=View.INVISIBLE
            tvParking.visibility=View.VISIBLE
            imParking.visibility=View.VISIBLE
            counter=0

            rvAdapter.notifyDataSetChanged()
            toolbar.setBackgroundColor(ContextCompat.getColor(this@MainActivity,R.color.purple_700))
            toolbar.menu.clear()
            toolbar.inflateMenu(R.menu.search_menu)
            tvCounter.text=defaultText
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
        selectionList.clear()
        selectionListPosition.clear()
    }
    override fun onLongClick(p0: View?): Boolean {
        isContexualModeEnabled=true
        binding.toolbar.menu.clear()
        binding.toolbar.inflateMenu(R.menu.contexual_menu)
        supportActionBar!!.title=defaultText

        binding.tvParking.visibility=View.INVISIBLE
        binding.imParking.visibility=View.INVISIBLE
        binding.tvCounter.visibility=View.VISIBLE

        binding.tvCounter.text=defaultText
        binding.toolbar.setBackgroundColor(ContextCompat.getColor(this,R.color.main_color))
        rvAdapter.notifyDataSetChanged()
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        return true
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        return super.onContextItemSelected(item)
    }
}
