package com.example.recyclerviewswipedloadjson

interface OnItemClickListener {
    fun onItemClicked(position:Int){}
}