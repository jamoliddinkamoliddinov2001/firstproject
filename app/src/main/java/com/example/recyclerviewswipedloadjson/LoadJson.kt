package com.example.recyclerviewswipedloadjson

import android.content.Context
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets


object JavaFunctions {
    @Throws(IOException::class)
    fun ReadFile(fileName: String?, context: Context): String {
        var reader: BufferedReader? = null
        reader = BufferedReader(
            InputStreamReader(
                context.assets.open(fileName!!),
                StandardCharsets.UTF_8
            )
        )
        val content = StringBuilder()
        var line: String?
        while (reader.readLine().also { line = it } != null) {
            content.append(line)
        }
        return content.toString()
    }
}