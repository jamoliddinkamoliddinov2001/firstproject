package com.example.recyclerviewswipedloadjson

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.collections.ArrayList

class RvAdapter(private val list: MutableList<Item>, activity: MainActivity) : RecyclerView.Adapter<RvAdapter.viewHolder>(),Filterable {
    private var itemList : MutableList<Item>
    private var mainActivity:MainActivity

    init {
        itemList = list
        mainActivity=activity
    }

    inner class viewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView),View.OnClickListener {
        val tvCar = itemView.findViewById<TextView>(R.id.tv_car)
        val tvName = itemView.findViewById<TextView>(R.id.tv_name)
        val tvEmail = itemView.findViewById<TextView>(R.id.tv_email)
        val tvGender = itemView.findViewById<TextView>(R.id.tv_gender)
        val checkBox=itemView.findViewById<CheckBox>(R.id.check_box)
        var view=itemView

        init {
            view.setOnLongClickListener(mainActivity)
            checkBox.setOnClickListener(this)
        }

        @SuppressLint("SetTextI18n")
        fun bind(item: Item) {

            tvCar.text = item.car
            if (item.email.length>=23){
            val result=item.email.substring(0,23)
            tvEmail.text="$result..."
            } else tvEmail.text=item.email
            tvName.text="${item.firstName} ${item.lastName}"
            tvGender.text=item.gender
            if (mainActivity.isContexualModeEnabled){
                checkBox.visibility=View.VISIBLE
                checkBox.isChecked=false
            } else{
                checkBox.visibility=View.INVISIBLE
            }
        }

        override fun onClick(p0: View) {
            mainActivity.selectItem(p0,adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        return viewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
    holder.bind(itemList[position])
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
    fun removeItem(selectionList:MutableList<Item>){
        for (i in selectionList.indices){
            itemList.remove(selectionList[i])
            notifyDataSetChanged()
        }
    }

    override fun getFilter(): Filter {
        return object :Filter(){
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val queryText=p0.toString()
                if (queryText.isEmpty()){
                    itemList=list
                }else{
                    val resultList:MutableList<Item> = ArrayList<Item>()
                    for (row in list){
                        if (row.car.toLowerCase(Locale.ROOT).contains(queryText.toLowerCase(Locale.ROOT))){
                            resultList.add(row)
                        }
                    }
                    itemList=resultList
                }
                val filterResult=FilterResults()
                filterResult.values=itemList
                return filterResult
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                (p1?.values as MutableList<Item>).also { itemList = it }
                notifyDataSetChanged()
            }

        }
    }
}